﻿namespace NimbusFox.BackupPlus.Classes {
    public class Message {
        public string TranslationCode { get; set; }
        public object[] Data { get; set; }
        public bool AdminOnly { get; set; }
    }
}
