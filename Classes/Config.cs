﻿using System.Linq;
using Plukit.Base;

namespace NimbusFox.BackupPlus.Classes {
    public class Config {
        public bool BackupOnStart { get; private set; }
        public bool BackUpOnInterval { get; private set; } = true;
        public uint BackUpIntervalMinutes { get; private set; } = 120;
        public string Method { get; private set; } = "folder";
        public string Location { get; private set; } = "backups/";
        public string BackUpPassword { get; private set; } = "";
        public string ftpUserName { get; private set; } = "";
        public string ftpHost { get; private set; } = "";
        public string ftpPassword { get; private set; } = "";
        public uint ftpPort { get; private set; } = 0;
        public uint oldBackups { get; private set; } = 4;

        private string[] _validMethods = { "folder", "ftp", "sftp", "ftps" };

        public void Restore(Blob config) {
            var backup = config.FetchBlob("backup");

            BackupOnStart = backup.GetBool("onStart", false);
            BackUpOnInterval = backup.GetBool("onInterval", true);

            if (uint.TryParse(config.GetLong("intervalMinutes", BackUpIntervalMinutes).ToString(), out var minutes)) {
                BackUpIntervalMinutes = minutes;
            }

            Method = config.GetString("method", "folder").ToLower();

            if (!_validMethods.Contains(Method)) {
                Method = "folder";
                Location = "backups/";
            }

            if (config.Contains("backupPassword")) {
                BackUpPassword = config.GetString("backupPassword");
            }

            if ((Method == "ftp" || Method == "sftp" || Method == "ftps") && config.Contains("(s)ftp(s)")) {
                var ftp = config.FetchBlob("(s)ftp(s)");

                ftpUserName = ftp.GetString("username", "");
                ftpPassword = ftp.GetString("password", "");
                ftpHost = ftp.GetString("host", "");
                if (uint.TryParse(ftp.GetLong("port", 0).ToString(), out var port)) {
                    if (port <= 65535) {
                        ftpPort = port;
                    }
                }
            }

            Location = config.GetString("location", Location);

            if (uint.TryParse(config.GetLong("oldBackups", 4).ToString(), out var amount)) {
                oldBackups = amount;
            }
        }

        public Blob Save() {
            var blob = BlobAllocator.Blob(true);

            var backup = blob.FetchBlob("backup");
            backup.SetBool("onStart", BackupOnStart);
            backup.SetBool("onInterval", BackUpOnInterval);

            blob.SetLong("intervalMinutes", BackUpIntervalMinutes);
            blob.SetString("methodOptions_comment", "folder, ftp, ftps, sftp");
            blob.SetString("method", Method);
            blob.SetString("backupPassword", BackUpPassword);
            blob.SetString("location_comment_1", "If using the folder option it will place the folder from the root Staxel folder. So if loading from steam it's directory will be 'Staxel/(target directory)'");
            blob.SetString("location_comment_2", "If you would like to store the folder in a different drive or start from the linux root directory place (drive letter):\\ for windows or start the text with '/'");
            blob.SetString("location", Location);

            var ftp = blob.FetchBlob("(s)ftp(s)");
            ftp.SetString("username", ftpUserName.IsNullOrEmpty() ? "username" : ftpUserName);
            ftp.SetString("password", ftpPassword.IsNullOrEmpty() ? "password" : ftpPassword);
            ftp.SetString("host", ftpHost.IsNullOrEmpty() ? "host" : ftpHost);
            ftp.SetLong("port", ftpPort == 0 ? 21 : ftpPort);

            blob.SetString("oldBackups_comment", "This how many backups are kept before deleting them");
            blob.SetLong("oldBackups", oldBackups);

            return blob;
        }
    }
}
