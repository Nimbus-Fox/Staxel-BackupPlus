﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Authentication;
using System.Threading;
using FluentFTP;
using Ionic.Zip;
using Ionic.Zlib;
using NimbusFox.BackupPlus.Classes;
using Plukit.Base;
using Renci.SshNet;
using Staxel;
using Staxel.Behavior;
using Staxel.Collections;
using Staxel.Core;
using Staxel.Items;
using Staxel.Items.Pricing;
using Staxel.Logic;
using Staxel.Mail;
using Staxel.Modding;
using Staxel.Server;
using Staxel.Storage;
using Staxel.TileBlobStorage;
using Staxel.Tiles;

namespace NimbusFox.BackupPlus {
    public class BackupHook : IModHookV4 {
        private DirectoryInfo _modFolder;
        private DirectoryInfo _contentFolder;
        private DirectoryInfo _backupFolder;
        private ServerMainLoop _serverMainLoop;
        private Config _config;
        private DateTime _lastBackup;
        internal static bool ForceRunBackup = false;
        private readonly List<Message> _messages = new List<Message>();

        public void Dispose() { }
        public void GameContextInitializeInit() { }
        public void GameContextInitializeBefore() { }

        public BackupHook() {

        }

        public void GameContextInitializeAfter() {
            _modFolder = new DirectoryInfo(Path.Combine(new DirectoryInfo(GameContext.ContentLoader.RootDirectory).Parent.FullName, "modConfigs", "BackupPlus"));
            _contentFolder = new DirectoryInfo(GameContext.ContentLoader.LocalContentDirectory);

            if (!_modFolder.Exists) {
                _modFolder.Create();
            }

            _config = new Config();

            if (!_modFolder.GetFiles().Any(x => x.Name == "backup.json")) {
                var stream = new MemoryStream();
                _config.Save().SaveJsonStream(stream);
                File.WriteAllBytes(Path.Combine(_modFolder.FullName, "backup.json"), stream.ToArray());
                stream.Dispose();
            }

            var mem = new MemoryStream(File.ReadAllBytes(Path.Combine(_modFolder.FullName, "backup.json")));

            var blob = BlobAllocator.Blob(true);

            blob.LoadJsonStream(mem);

            _config.Restore(blob);
            using (var stream = new MemoryStream()) {
                _config.Save().SaveJsonStream(stream);
                File.WriteAllBytes(Path.Combine(_modFolder.FullName, "backup.json"), stream.ToArray());
            }

            mem.Close();
            mem.Dispose();

            Blob.Deallocate(ref blob);

            if (_config.Method == "folder") {
                if (ClrDetection.IsMono()) {
                    if (_config.Location[0] == '/') {
                        _backupFolder = new DirectoryInfo(_config.Location);
                        _backupFolder.Create();
                    }
                } else {
                    if (_config.Location[1] == ':') {
                        _backupFolder = new DirectoryInfo(_config.Location);
                        _backupFolder.Create();
                    }
                }

                if (_backupFolder == null) {
                    _backupFolder = _contentFolder.Parent.Parent.CreateSubdirectory(_config.Location);
                }
            }

            if (_config.BackupOnStart) {
                Backup(true);
            }

            _lastBackup = DateTime.Now;
        }

        public void GameContextDeinitialize() {
        }
        public void GameContextReloadBefore() { }
        public void GameContextReloadAfter() { }

        public void UniverseUpdateBefore(Universe universe, Timestep step) {
            if (!universe.Server) {
                return;
            }

            if (_serverMainLoop == null) {
                while (_serverMainLoop == null && ServerContext.VillageDirector != null && ServerContext.VillageDirector.HasDirectorFacade()) {
                    _serverMainLoop = (ServerMainLoop)typeof(DirectorUniverseFacade)
                        .GetField("_serverMainLoop",
                            BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                        ?.GetValue(ServerContext.VillageDirector.UniverseFacade);
                }
            }

            foreach (var message in new List<Message>(_messages)) {
                SendMessage(universe, message.TranslationCode, message.Data, message.AdminOnly);
                _messages.Remove(message);
            }

            Backup(universe);
        }
        public void UniverseUpdateAfter() { }
        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            return true;
        }

        private object GetPrivatePropertyValue(string field, object parentObject, Type type) {
            return type.GetProperty(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)?
                .GetValue(parentObject);
        }

        private void SetPrivatePropertyValue(string field, object parentObject, Type type, object value) {
            type.GetProperty(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)?.SetValue(parentObject, value);
        }

        private object GetPrivateFieldValue(string field, object parentObject, Type type) {
            return type.GetField(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)?
                .GetValue(parentObject);
        }

        private void SetPrivateFieldValue(string field, object parentObject, Type type, object value) {
            type.GetField(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)?.SetValue(parentObject, value);
        }

        private Stopwatch _sw = new Stopwatch();

        private void Backup(EntityUniverseFacade universe) {
            if (_serverMainLoop != null) {
                if (_config.BackUpOnInterval || ForceRunBackup) {
                    if (new TimeSpan(DateTime.Now.Ticks - _lastBackup.Ticks).TotalMinutes >=
                        _config.BackUpIntervalMinutes || ForceRunBackup) {
                        _lastBackup = DateTime.Now;
                        SendMessage(universe, "nimbusfox.backupPlus.startingBackup", new object[0]);
                        _sw.Reset();
                        _sw.Start();
                        try {
                            while (_backupFolder.GetFiles().Count(x => x.Name.StartsWith(_contentFolder.Name)) > _config.oldBackups) {
                                var oldest = (FileInfo) null;

                                foreach (var dir in _backupFolder.GetFiles().Where(x => x.Name.StartsWith(_contentFolder.Name))) {
                                    if (oldest == null) {
                                        oldest = dir;
                                        continue;
                                    }

                                    if (oldest.CreationTime > dir.CreationTime) {
                                        oldest = dir;
                                    }
                                }

                                oldest?.Delete();
                            }
                            Backup(true, stream => {

                                var bytes = stream.Length;

                                _messages.Add(new Message {
                                    AdminOnly = true,
                                    Data = new object[] { BytesToStorage(bytes) },
                                    TranslationCode = "nimbusfox.backupPlus.success"
                                });
                            });
                        } catch (Exception ex) when (!Debugger.IsAttached) {
                            _sw.Stop();
                            _messages.Add(new Message {
                                AdminOnly = false,
                                Data = new object[] { $"{_sw.Elapsed.Hours:00}:{_sw.Elapsed.Minutes:00}:{_sw.Elapsed.Seconds:00}" },
                                TranslationCode = "nimbusfox.backupPlus.finishedBackup"
                            });
                            Logger.LogException(ex);
                            SendMessage(universe, "nimbusfox.backupPlus.error", new object[0], true);
                        } finally {
                            ForceRunBackup = false;
                        }
                    }
                }
            }

            ForceRunBackup = false;
        }

        private static List<Stream> _streams = new List<Stream>();

        private void Backup(bool live = false, Action<Stream> onFinish = null) {
            if (_contentFolder.Name.ToLower() == "localcontent") {
                return;
            }

            var zip = new ZipFile();
            if (!_config.BackUpPassword.IsNullOrEmpty()) {
                zip.Password = _config.BackUpPassword;
            }

            zip.CompressionLevel = CompressionLevel.BestSpeed;
            zip.CompressionMethod = CompressionMethod.BZip2;

            var bindings = (Dictionary<string, BlobBinding>)GetPrivateFieldValue("_bindings", ServerContext.BlobStorage, typeof(BlobStorage));

            var ignores = new List<string>();

            foreach (var binding in bindings.Values) {
                var file = (string)GetPrivateFieldValue("_file", binding, typeof(BlobBinding));
                var blob = binding.Get();

                var path = file.Replace(GameContext.ContentLoader.LocalContentDirectory, "");

                var ms = new MemoryStream();
                blob.WriteFull(ms);
                ms.Seek(0L, SeekOrigin.Begin);
                var output = new MemoryStream();
                output.WriteSubStream(ms);
                output.Seek(0L, SeekOrigin.Begin);
                zip.AddEntry(path, output);
                ignores.Add(new FileInfo(file).Name);
                _streams.Add(ms);

                Logger.WriteLine("Backing up /" + path);
            }

            var storage = GameContext.Resources.FetchBlob("storage.config").FetchBlob("activity");

            var stores = (IDictionary)GetPrivateFieldValue("_stores",
                ServerContext.ChunkActivityDatabase, typeof(ChunkActivityDatabase));

            var keys = stores.Keys.Cast<string>().ToArray();
            var values = stores.Values.Cast<object>().ToArray();

            for (var i = 0; i < stores.Count; i++) {
                var key = keys[i];
                var value = values[i];

                var config = storage.FetchBlob(key);

                var sto = config.FetchBlob("storage");

                var engine = sto.GetString("engine", "none");

                if (engine != "chunklog") {
                    continue;
                }

                var store = GetPrivateFieldValue("_storage", value, value.GetType());

                var current = (ChunkKeySet)GetPrivateFieldValue("_current", store, store.GetType());

                var writeBuffer = (byte[])GetPrivateFieldValue("_writeBuffer", store, store.GetType());

                var ms = new MemoryStream();

                foreach (var ikey in current) {
                    ms.WriteULong(ikey.ToPacked() << 1 | 1UL, writeBuffer);
                }

                ms.Seek(0L, SeekOrigin.Begin);

                zip.AddEntry(sto.GetString("file"), ms);
                ignores.Add(sto.GetString("file"));
                Logger.WriteLine("Backing up /" + sto.GetString("file"));
                _streams.Add(ms);

            }

            CycleDirectories(zip, _contentFolder, ignores.ToArray());

            var thread = new Thread(() => {
                try {
                    var fileName = _contentFolder.Name + "-" +
                                   DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + ".zip";
                    using (var stream = new FileStream(Path.Combine(_backupFolder.FullName, fileName), FileMode.Create)) {
                        zip.Save(stream);

                        zip.Dispose();

                        stream.Seek(0, SeekOrigin.Begin);

                        if (_config.Method == "ftp") {
                            FtpSave(stream, fileName);
                        } else if (_config.Method == "ftps") {
                            FtpsSave(stream, fileName);
                        } else if (_config.Method == "sftp") {
                            SftpSave(stream, fileName);
                        }

                        _sw.Stop();
                        _messages.Add(new Message {
                            AdminOnly = false,
                            Data = new object[] { $"{_sw.Elapsed.Hours:00}:{_sw.Elapsed.Minutes:00}:{_sw.Elapsed.Seconds:00}" },
                            TranslationCode = "nimbusfox.backupPlus.finishedBackup"
                        });

                        foreach (var streamCache in _streams) {
                            streamCache.Dispose();
                        }

                        _streams.Clear();

                        onFinish?.Invoke(stream);
                    }
                } finally {
                    // ignore
                }
            });

            thread.Start();

            if (!live) {
                while (thread.IsAlive) { }
            }
        }

        private void CycleDirectories(ZipFile file, DirectoryInfo dir, string[] ignoreFiles, string path = "/") {
            if (path != "/") {
                try {
                    file.AddDirectory(dir.FullName, path);
                } catch {
                    // stops exception
                }
            }

            foreach (var directory in dir.GetDirectories()) {
                CycleDirectories(file, directory, ignoreFiles, path + directory.Name + "/");
            }

            foreach (var f in dir.GetFiles()) {
                if (f.Name.StartsWith("lod") || ignoreFiles.Contains(f.Name)) {
                    continue;
                }

                Logger.WriteLine("Backing up " + path + f.Name);
                if (!ClrDetection.IsMono()) {
                    var stream = new FileStream(f.FullName, FileMode.Open, FileAccess.Read,
                        FileShare.ReadWrite);
                    try {
                        file.AddEntry((path == "/" ? "" : path) + f.Name, stream);
                        _streams.Add(stream);
                    } catch {
                        // ignore
                    }
                }
            }
        }

        private void SendMessage(EntityUniverseFacade universe, string translationCode, object[] paramsObjects, bool adminsOnly = false) {
            var players = new Lyst<Entity>();

            universe.GetPlayers(players);

            foreach (var player in players) {
                if (!adminsOnly || player.PlayerEntityLogic.IsAdmin()) {
                    _serverMainLoop.MessagePlayer(player.PlayerEntityLogic.Uid(), translationCode, paramsObjects);
                }
            }
        }

        private string BytesToStorage(long bytes) {
            var process = (double)bytes;

            var loop = 0;

            var values = new[] { "B", "KB", "MB", "GB" };

            while (process > 1024) {
                process = process / 1024;

                loop++;

                if (loop == values.Length - 1) {
                    break;
                }
            }

            return $"{process:N2} {values[loop]}";
        }

        private void FtpSave(Stream stream, string fileName) {
            var ftpClient = new FtpClient(_config.ftpHost, (int)_config.ftpPort,
                new NetworkCredential(_config.ftpUserName, _config.ftpPassword));

            var path = (_config.Location.StartsWith("/") ? "" : "/") + _config.Location +
                       (_config.Location.EndsWith("/") ? "" : "/") + fileName;

            try {
                ftpClient.Connect();

                try {
                    if (ftpClient.FileExists(path)) {
                        goto ftpDisconnect;
                    }
                } catch {
                    //ignore
                }

                ftpClient.Upload(stream, path, FtpExists.NoCheck, true, new Progress<double>(
                    value => {
                        Logger.WriteLine("Uploaded " + value + "% of " + fileName);
                    }));

                ftpDisconnect:

                ftpClient.Disconnect();

                ftpClient.Dispose();
            } catch (Exception ex) {
                Logger.LogException(ex);
                _messages.Add(new Message {
                    AdminOnly = true,
                    Data = new object[0],
                    TranslationCode = "nimbusfox.backupPlus.error"
                });
            } finally {
                ftpClient.Dispose();
            }
        }

        private void FtpsSave(Stream stream, string fileName) {
            var ftpClient = new FtpClient(_config.ftpHost, (int)_config.ftpPort,
                new NetworkCredential(_config.ftpUserName, _config.ftpPassword)) {
                SslProtocols = SslProtocols.Default
            };

            var path = (_config.Location.StartsWith("/") ? "" : "/") + _config.Location +
                       (_config.Location.EndsWith("/") ? "" : "/") + fileName;

            try {
                ftpClient.Connect();

                try {
                    if (ftpClient.FileExists(path)) {
                        goto ftpsDisconnect;
                    }
                } catch {
                    //ignore
                }

                var max = 0;

                ftpClient.Upload(stream, path, FtpExists.NoCheck, true, new Progress<double>(
                    value => {
                        if (max < Math.Floor(value)) {
                            max = (int)Math.Floor(value);
                            Logger.WriteLine("Uploaded " + max + "% of " + fileName);
                        }
                    }));

                ftpsDisconnect:

                ftpClient.Disconnect();

                ftpClient.Dispose();
            } catch (Exception ex) {
                Logger.LogException(ex);
                _messages.Add(new Message {
                    AdminOnly = true,
                    Data = new object[0],
                    TranslationCode = "nimbusfox.backupPlus.error"
                });
            } finally {
                ftpClient.Dispose();
            }
        }

        private void SftpSave(Stream stream, string fileName) {
            var sftpClient = new SftpClient(_config.ftpHost, (int)_config.ftpPort, _config.ftpUserName,
                _config.ftpPassword);
            try {

                sftpClient.Connect();

                var max = 0UL;

                sftpClient.UploadFile(stream,
                    (_config.Location.StartsWith("/") ? "" : "/") + _config.Location +
                    (_config.Location.EndsWith("/") ? "" : "/") + fileName, false, value => {
                        if (max < value) {
                            max = value;
                            Logger.WriteLine("Uploaded " + BytesToStorage((long)value) + " of " + fileName);
                        }
                    });

                sftpClient.Disconnect();
            } catch (Exception ex) {
                Logger.LogException(ex);
                _messages.Add(new Message {
                    AdminOnly = true,
                    Data = new object[0],
                    TranslationCode = "nimbusfox.backupPlus.error"
                });
            } finally {
                sftpClient.Dispose();
            }
        }

        /// <inheritdoc />
        public void ClientContextInitializeInit() { }

        /// <inheritdoc />
        public void ClientContextInitializeBefore() { }

        /// <inheritdoc />
        public void ClientContextInitializeAfter() { }

        /// <inheritdoc />
        public void ClientContextDeinitialize() { }

        /// <inheritdoc />
        public void ClientContextReloadBefore() { }

        /// <inheritdoc />
        public void ClientContextReloadAfter() { }

        /// <inheritdoc />
        public void CleanupOldSession() { }

        /// <inheritdoc />
        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            return true;
        }

        /// <inheritdoc />
        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return true;
        }
    }
}
