﻿using Plukit.Base;
using Staxel.Commands;
using Staxel.Server;

namespace NimbusFox.BackupPlus.Commands {
    public class BackupCommand : ICommandBuilder {
        public string Execute(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };
            BackupHook.ForceRunBackup = true;
            return "";
        }

        public string Kind => "backup";
        public string Usage => "nimbusfox.backupPlus.command.description";
        public bool Public => false;
    }
}
